#![feature(rustc_private)]

extern crate rustc_ast;
extern crate rustc_driver;
extern crate rustc_errors;
extern crate rustc_hir;
extern crate rustc_interface;
extern crate rustc_lint;
extern crate rustc_span;

struct MyLint;

impl rustc_lint::LintPass for MyLint {
    fn name(&self) -> &'static str {
        "the best lint!"
    }
}

impl<'tcx> rustc_lint::LateLintPass<'tcx> for MyLint {
    #[inline(always)]
    fn check_body(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Body<'tcx>) {}

    #[inline(always)]
    fn check_body_post(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Body<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_crate(&mut self, _: &rustc_lint::LateContext<'tcx>) {}

    #[inline(always)]
    fn check_crate_post(&mut self, _: &rustc_lint::LateContext<'tcx>) {}

    #[inline(always)]
    fn check_mod(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Mod<'tcx>,
        _: rustc_hir::HirId,
    ) {
    }

    #[inline(always)]
    fn check_foreign_item(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::ForeignItem<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_item(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Item<'tcx>) {}

    #[inline(always)]
    fn check_item_post(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Item<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_local(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Local<'tcx>) {}

    #[inline(always)]
    fn check_block(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Block<'tcx>) {}

    #[inline(always)]
    fn check_block_post(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Block<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_stmt(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Stmt<'tcx>) {}

    #[inline(always)]
    fn check_arm(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Arm<'tcx>) {}

    #[inline(always)]
    fn check_pat(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Pat<'tcx>) {}

    #[inline(always)]
    fn check_expr(
        &mut self,
        cx: &rustc_lint::LateContext<'tcx>,
        expr: &'tcx rustc_hir::Expr<'tcx>,
    ) {
        // TODO: implement this
    }

    #[inline(always)]
    fn check_expr_post(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Expr<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_ty(&mut self, _: &rustc_lint::LateContext<'tcx>, _: &'tcx rustc_hir::Ty<'tcx>) {}

    #[inline(always)]
    fn check_generic_param(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::GenericParam<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_generics(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Generics<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_poly_trait_ref(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::PolyTraitRef<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_fn(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: rustc_hir::intravisit::FnKind<'tcx>,
        _: &'tcx rustc_hir::FnDecl<'tcx>,
        _: &'tcx rustc_hir::Body<'tcx>,
        _: rustc_span::Span,
        _: rustc_span::def_id::LocalDefId,
    ) {
    }

    #[inline(always)]
    fn check_trait_item(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::TraitItem<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_impl_item(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::ImplItem<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_impl_item_post(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::ImplItem<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_struct_def(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::VariantData<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_field_def(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::FieldDef<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_variant(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_hir::Variant<'tcx>,
    ) {
    }

    #[inline(always)]
    fn check_path(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &rustc_hir::Path<'tcx>,
        _: rustc_hir::HirId,
    ) {
    }

    #[inline(always)]
    fn check_attribute(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx rustc_ast::Attribute,
    ) {
    }

    #[inline(always)]
    fn enter_lint_attrs(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx [rustc_ast::Attribute],
    ) {
    }

    #[inline(always)]
    fn exit_lint_attrs(
        &mut self,
        _: &rustc_lint::LateContext<'tcx>,
        _: &'tcx [rustc_ast::Attribute],
    ) {
    }
}

struct MyCallbacks;

impl rustc_driver::Callbacks for MyCallbacks {
    fn config(&mut self, config: &mut rustc_interface::interface::Config) {
        config.register_lints = Some(Box::new(|_, ls| {
            ls.register_late_pass(|_| Box::new(MyLint))
        }));
    }

    // fn after_crate_root_parsing<'tcx>(
    //     &mut self,
    //     _compiler: &rustc_interface::interface::Compiler,
    //     _queries: &'tcx rustc_interface::Queries<'tcx>,
    // ) -> rustc_driver_impl::Compilation {
    //     rustc_driver_impl::Compilation::Continue
    // }
    //
    // fn after_expansion<'tcx>(
    //     &mut self,
    //     _compiler: &rustc_interface::interface::Compiler,
    //     _queries: &'tcx rustc_interface::Queries<'tcx>,
    // ) -> rustc_driver_impl::Compilation {
    //     rustc_driver_impl::Compilation::Continue
    // }
    //
    // fn after_analysis<'tcx>(
    //     &mut self,
    //     _compiler: &rustc_interface::interface::Compiler,
    //     _queries: &'tcx rustc_interface::Queries<'tcx>,
    // ) -> rustc_driver_impl::Compilation {
    //     rustc_driver_impl::Compilation::Continue
    // }
}

// fn main() -> Result<(), &'static str> {
fn main() -> Result<(), rustc_errors::ErrorGuaranteed> {
    println!("Hello, world!");
    let args: Vec<_> = std::env::args().collect();
    let mut my_cb = MyCallbacks;
    rustc_driver::RunCompiler::new(&args, &mut my_cb).run()
}
